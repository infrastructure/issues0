Issues Regarding the DUNE Development Infrastructure
====================================================

In this project you can discuss general issues with the DUNE development infrastructure and ways to do things (tm),
e.g.

* this GitLab server
* our mailing lists at https://lists.dune-project.org
* the https://dune-project.org website
* workflows for merge requests etc.
* Coding guidelines
* and whatever else comes to your mind (as long as it's connected to the infrastructure...)

Go to the [issue tracker](/infrastructure/issues/issues) and start discussing!